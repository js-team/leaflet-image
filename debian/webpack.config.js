'use strict';

var fs = require('fs');
var path = require('path');
var webpack = require('webpack');

var config = {

	target: 'web',
	resolve: {
		modules: ['/usr/lib/nodejs', '/usr/share/nodejs'],
	},
	resolveLoader: {
		modules: ['/usr/lib/nodejs', '/usr/share/nodejs'],
	},
	node: {
		fs: 'empty'
	},
	devtool: 'source-map',
	output: {
		libraryTarget: 'umd'
	},
}

module.exports = config;
